-- A
SELECT * FROM artists WHERE name LIKE "%d%";

-- B
SELECT * FROM songs WHERE length < 330;

-- C
SELECT albums.album_title AS album_name, songs.song_name AS song_name, songs.length
	FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- D
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id
	WHERE albums.album_title LIKE "%a%";

-- E
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- F
SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id
	ORDER BY albums.album_title DESC;

-- G
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id
	WHERE artists.name LIKE "%a%";
