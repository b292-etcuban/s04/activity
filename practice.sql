CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR (50) NOT NULL,
	full_name VARCHAR (50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR (50) NOT NULL,
	address VARCHAR (50) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE reviews (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	review VARCHAR (300) NOT NULL,
	datetime_created DATETIME NOT NULL,
	rating INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_reviews_user_id
		FOREIGN KEY (user_id) REFERENCES reviews(id)
);


INSERT INTO users(username, password, full_name, contact_number, email, address) VALUES ('ezrealAD', 'ez', 'Jarro Lightfeather', 2147483647, 'ez@league.com', 'Piltover');
INSERT INTO users(username, password, full_name, contact_number, email, address) VALUES ('seraphine', 'password123', 'Seraphine', 2147483647, 'seraphine@riot.com', 'Iona');
INSERT INTO users(username, password, full_name, contact_number, email, address) VALUES ('lux', 'lux123', 'Luxana Crownguard', 2147483647, 'lux@demacia.com', 'Demacia');
INSERT INTO users(username, password, full_name, contact_number, email, address) VALUES ('viktor', '1234viktor', 'Viktor', 2147483647, 'viktor@zaun.com', 'Zaun');
INSERT INTO users(username, password, full_name, contact_number, email, address) VALUES ('jinx', 'jinx123', 'Jinx', 2147483647, 'jinx@zaun.com', 'Zaun');

SELECT *FROM users;

INSERT INTO reviews(user_id, review, datetime_created, rating) VALUES (1, 'The songs are okay. Worth the subscription', '2023-05-03 00:00:00', 5);
INSERT INTO reviews(user_id, review, datetime_created, rating) VALUES (2, 'The songs are meh. I want BLACKPINK', '2023-01-03 00:00:00', 1);
INSERT INTO reviews(user_id, review, datetime_created, rating) VALUES (3, 'Add Bruno Mars and Lady Gaga', '2023-03-23 00:00:00', 4);
INSERT INTO reviews(user_id, review, datetime_created, rating) VALUES (4, 'I want to listen to more k-pop', '2022-09-23 00:00:00', 3);
INSERT INTO reviews(user_id, review, datetime_created, rating) VALUES (5, 'Kindly add more OPM', '2023-02-01 00:00:00', 5);

SELECT *FROM reviews;

SELECT users.*, reviews.* FROM reviews
	JOIN users ON reviews.user_id = users.id
	WHERE users.full_name LIKE '%x%';

SELECT reviews.*, users.* FROM reviews
	JOIN users ON reviews.user_id = users.id;


SELECT reviews.review, users.username AS username FROM reviews
	JOIN users ON reviews.user_id = users.id;


